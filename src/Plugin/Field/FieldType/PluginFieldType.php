<?php

namespace Drupal\plugin_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'plugin_field_type' field type.
 *
 * @FieldType(
 *   id = "plugin_field_type",
 *   label = @Translation("Plugin field"),
 *   category = @Translation("Plugin Field"),
 *   description = @Translation("Allows you to select a plugin from a service"),
 *   default_widget = "plugin_field_select_widget",
 *   default_formatter = "plugin_field_formatter"
 * )
 */
class PluginFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'plugin_service' => 'plugin.manager.plugin_field_widget',
        'plugin_widget' => 'plugin_field_select_widget',
      ] + parent::defaultFieldSettings();
  }

  /**
   * @return array
   */
  public function getWidgetOptions() {
    $plugin_manager = \Drupal::service('plugin.manager.plugin_field_widget');
    $plugin_definitions = $plugin_manager->getDefinitions();
    $ret = [];
    foreach ($plugin_definitions as $pd) {
      $label = isset($pd['label']) ? $pd['label'] : $pd['id'];
      $label = isset($pd['title']) ? $pd['title'] : $label;
      $ret[$pd['id']] = $label;
    }
    asort($ret);
    return $ret;
  }

  /**
   * @return array
   */
  public function getPluginList() {
    $services = \Drupal::getContainer()->getServiceIds();
    $ret = [];
    foreach ($services as $service) {
      if (strpos($service, 'plugin.manager') !== FALSE) {
        $ret[$service] = $service;
      }
    }
    asort($ret);
    return $ret;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['plugin_service'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Plugin service'),
      '#description' => t('Text of the plugin service name. Usually of the form plugin.manager.[plugin_machine_name]'),
      '#options' => $this->getPluginList(),
      '#default_value' => $this->getSetting('plugin_service'),
    ];

    $element['plugin_widget'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Plugin widget'),
      '#description' => t('The plugin widget for choosing how to list options'),
      '#options' => $this->getWidgetOptions(),
      '#default_value' => $this->getSetting('plugin_widget'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Plugin'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  protected static function castAllowedValue($value) {
    return (string) $value;
  }

}
