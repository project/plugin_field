<?php

namespace Drupal\plugin_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'plugin_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "plugin_field_formatter",
 *   label = @Translation("Plugin field formatter"),
 *   field_types = {
 *     "plugin_field_type"
 *   }
 * )
 */
class PluginFieldFormatter extends FormatterBase {

  /**
   * @return array
   */
  public function getDisplayOptions() {
    $plugin_manager = \Drupal::service('plugin.manager.plugin_field_display');
    $plugin_definitions = $plugin_manager->getDefinitions();
    $ret = [];
    foreach ($plugin_definitions as $pd) {
      $ret[$pd['id']] = $pd['label'];
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'plugin_display' => 'plugin_id',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'plugin_display' => [
        '#type' => 'select',
        '#title' => t('Plugin display'),
        '#description' => t('The plugin for showing the selected plugin'),
        '#options' => $this->getDisplayOptions(),
        '#default_value' => $this->getSetting('plugin_display'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $options = $this->getDisplayOptions();
    $plugin = $options[$this->getSetting('plugin_display')];
    $summary = [t('Display: %plugin', ['%plugin' => $plugin])];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {

    $val = $item->getValue();
    if (isset($val['value'])) {
      $plugin_manager = \Drupal::service('plugin.manager.plugin_field_display');
      $plugin_definitions = $plugin_manager->getDefinitions();
      $entity = $item->getEntity();
      $display_plugin = $this->getSetting('plugin_display');
      $field_definition = $this->fieldDefinition;

      return $plugin_definitions[$display_plugin]['class']::display($this, $field_definition, $entity, $val['value']);
    }
    else {
      return '';
    }
  }

}
