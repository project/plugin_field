<?php

namespace Drupal\plugin_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\plugin_field\Plugin\plugin_field\Widget\PluginFieldDefaultWidget;

/**
 * Plugin implementation of the 'plugin_field_widget' widget.
 */
class PluginFieldWidgetBase extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if ($this->multiple) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return t('- None -');
      }
    }
    else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return t('- None -');
      }
      if (!$this->has_value) {
        return t('- Select a value -');
      }
    }
  }

  /**
   * Gets the available options
   * @param FieldableEntityInterface $entity
   * @return array
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    $field_name = $this->fieldDefinition->getName();
    $service = $entity->getFieldDefinition($field_name)->getSetting('plugin_service');
    $widget = $entity->getFieldDefinition($field_name)->getSetting('plugin_widget');

    $plugin_manager = \Drupal::service('plugin.manager.plugin_field_widget');
    $plugin_definitions = $plugin_manager->getDefinitions();

    /**
     * In certain cases a bad default value is passed when the field is created.
     * If so, replace with "default."
     */
    if (isset($plugin_definitions[$widget]['class'])) {
      $class = $plugin_definitions[$widget]['class'];
      return $class::getOptions($service, $field_name, $entity);
    }
    else {
      $class = $plugin_definitions['default']['class'];
      return $class::getOptions($service, $field_name, $entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    return $element;
  }
}
