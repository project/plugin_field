<?php

namespace Drupal\plugin_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\plugin_field\Plugin\plugin_field\Widget\PluginFieldDefaultWidget;

/**
 * Plugin implementation of the 'plugin_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "plugin_field_checkbox_widget",
 *   label = @Translation("Plugin Field: checkboxes/radios"),
 *   field_types = {
 *     "plugin_field_type"
 *   },
 *   multiple_values = TRUE
 * )
 */
class PluginFieldCheckboxWidget extends PluginFieldWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element += [
      '#type' => $this->multiple ? 'checkboxes' : 'radios',
      '#options' => $this->getOptions($items->getEntity()),
      '#empty_option' => $this->getEmptyLabel(),
      '#default_value' => $this->getSelectedOptions($items),
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => $this->multiple && count($this->getOptions($items->getEntity())) > 1,
    ];

    return $element;
  }
}
