<?php

namespace Drupal\plugin_field\Plugin\plugin_field\Display;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\Entity\FieldConfig;
use Drupal\plugin_field\Plugin\Field\FieldFormatter\PluginFieldFormatter;
use Drupal\plugin_field\Plugin\Field\FieldWidget\PluginFieldWidget;
use Drupal\plugin_field\PluginFieldWidgetInterface;

/**
 * @PluginFieldDisplay(
 *   id = "plugin_class",
 *   label = @Translation("Plugin Class"),
 * )
 */
class PluginFieldClassDisplay extends PluginBase implements PluginFieldWidgetInterface {

  /**
   * @return TranslatableMarkup|string
   */
  public function description() {
    return $this->t('Default: Show the class of the selected plugin.');
  }

  /**
   * @param PluginFieldFormatter $field_formatter
   * @param FieldableEntityInterface $entity
   * @param $plugin
   * @return string
   */
  public static function display(FormatterBase $field_formatter, FieldConfig $field_definition, FieldableEntityInterface $entity, $plugin) {
    $plugin_service = $field_definition->getSetting('plugin_service');
    $plugin_manager = \Drupal::service($plugin_service);
    $plugin_definitions = $plugin_manager->getDefinitions();
    if (isset($plugin_definitions[$plugin]['class'])) {
      return $plugin_definitions[$plugin]['class'];
    }
    else {
      return '';
    }

  }
}
