<?php

namespace Drupal\plugin_field\Plugin\plugin_field\Display;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\Entity\FieldConfig;
use Drupal\plugin_field\Plugin\Field\FieldFormatter\PluginFieldFormatter;
use Drupal\plugin_field\Plugin\Field\FieldWidget\PluginFieldWidget;
use Drupal\plugin_field\PluginFieldWidgetInterface;

/**
 * @PluginFieldDisplay(
 *   id = "plugin_id",
 *   label = @Translation("Plugin ID"),
 * )
 */
class PluginFieldIdDisplay extends PluginBase implements PluginFieldWidgetInterface {

  /**
   * @return TranslatableMarkup|string
   */
  public function description() {
    return $this->t('Default: Show the class of the selected plugin.');
  }

  /**
   * @param FormatterBase $field_formatter
   * @param FieldConfig $field_definition
   * @param FieldableEntityInterface $entity
   * @param $plugin
   * @return string
   */
  public static function display(FormatterBase $field_formatter, FieldConfig $field_definition, FieldableEntityInterface $entity, $plugin) {
    return $plugin;
  }
}
