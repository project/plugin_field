<?php

namespace Drupal\plugin_field\Plugin\plugin_field\Widget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\plugin_field\Plugin\Field\FieldWidget\PluginFieldWidget;
use Drupal\plugin_field\PluginFieldWidgetInterface;

/**
 * @PluginFieldWidget(
 *   id = "default",
 *   label = @Translation("Default Widget"),
 * )
 */
class PluginFieldDefaultWidget extends PluginBase implements PluginFieldWidgetInterface {

  /**
   * @return TranslatableMarkup|string
   */
  public function description()
  {
    return $this->t('Default: render all items found by the plugin.');
  }

  /**
   * @param $plugin_service
   * @param $field_name
   * @param $entity
   * @return array
   */
  public static function getOptions($plugin_service, $field_name, FieldableEntityInterface $entity) {
    $service = $entity->getFieldDefinition($field_name)->getSetting('plugin_service');
    $plugin_manager = \Drupal::service($service);
    $plugin_definitions = $plugin_manager->getDefinitions();
    $ret = [];
    foreach ($plugin_definitions as $pd) {
      $label = isset($pd['label']) ? $pd['label'] : $pd['id'];
      $label = isset($pd['title']) ? $pd['title'] : $label;
      $ret[$pd['id']] = $label;
    }
    asort($ret);
    return $ret;
  }
}
