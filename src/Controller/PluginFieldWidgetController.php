<?php
/**
 * @file
 */

namespace Drupal\plugin_field\Controller;

use Drupal\plugin_field\PluginFieldWidgetManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PluginFieldWidgetController
 *
 * Provides the route and API controller for PluginFieldWidget.
 */
class PluginFieldWidgetController extends ControllerBase
{

  protected $PluginFieldWidgetManager; //The plugin manager.

  /**
   * Constructor.
   *
   * @param \Drupal\plugin_field\PluginFieldWidgetManager $plugin_manager
   */

  public function __construct(PluginFieldWidgetManager $plugin_manager) {
    $this->PluginFieldWidgetManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Use the service container to instantiate a new instance of our controller.
    return new static($container->get('plugin.manager.plugin_field_widget'));
  }
}
