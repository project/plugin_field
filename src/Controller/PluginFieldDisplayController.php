<?php
/**
 * @file
 */

namespace Drupal\plugin_field\Controller;

use Drupal\plugin_field\PluginFieldDisplayManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PluginFieldDisplayController
 *
 * Provides the route and API controller for PluginFieldDisplay.
 */
class PluginFieldDisplayController extends ControllerBase
{

  protected $PluginFieldDisplayManager; //The plugin manager.

  /**
   * Constructor.
   *
   * @param \Drupal\plugin_field\PluginFieldDisplayManager $plugin_manager
   */

  public function __construct(PluginFieldDisplayManager $plugin_manager) {
    $this->PluginFieldDisplayManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Use the service container to instantiate a new instance of our controller.
    return new static($container->get('plugin.manager.plugin_field_display'));
  }
}
