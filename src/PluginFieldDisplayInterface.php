<?php
/**
 * @file
 * Provides Drupal\plugin_field\PluginInterface;
 */
namespace Drupal\plugin_field;
/**
 * An interface for all PluginFieldDisplay type plugins.
 */
interface PluginFieldDisplayInterface {
  /**
   * Provide a description of the plugin.
   * @return string
   *   A string description of the plugin.
   */
  public function description();
}
