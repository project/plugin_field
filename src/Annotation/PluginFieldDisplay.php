<?php

namespace Drupal\plugin_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a PluginFieldDisplay annotation object.
 *
 * Plugin Namespace: Plugin\PluginFieldDisplay
 *
 * @see plugin_api
 *
 * @Annotation
 */
class PluginFieldDisplay extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the PluginFieldDisplay.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The category under which the PluginFieldDisplay should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

}
