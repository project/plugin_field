<?php

namespace Drupal\plugin_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a PluginFieldWidget annotation object.
 *
 * Plugin Namespace: Plugin\PluginFieldWidget
 *
 * @see plugin_api
 *
 * @Annotation
 */
class PluginFieldWidget extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the PluginFieldWidget.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The category under which the PluginFieldWidget should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

}
